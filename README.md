Chang'E-2 SWIDs and HPD data
   Chang'E-2 is a the second lunar orbiter of China. 
   Solar Wind Ion Detectors (SWIDs) and High Energetic Particles Detectors (HPD) on Chang’E-2 are the backup instruments 
   of Chang’E-1 .
   The 'ChangE-2.zip' is include the SWID data and HPD data.
   HPDdata:
   CE2_HPD_SCI_P201010_0.0B0_1_CSSAR.02
   CE2_HPD_SCI_P201109_0.0B0_1_CSSAR.02
   format:
   Beijing Time(year，month，day，hour，minute，second), P1,P2,P3,P4,P5,P6,E1,E2,He,Li,C (particles/cm2 sr sec MeV/nucleon)
   
   SWIDdata:
   2010/CE2_CSSAR_SWIDB_SCI_201010_ 2010 10 17_P12.01
   2011/CE2_CSSAR_SWIDB_SCI_201109_ 2011 9 27_P12.01
   format:
   Beijing Time(year，month，day，hour，minute，second), Channel E1-E48 (Counts)
   
   UTC=Beijing Time-8.
   
   Orbit:
   CE2_SWIDB20101711_ORBIT.txt
   Format:
   UTC,Selenocentric_Coor_X,Y,Z(Kilometer);Solar_Zenith_Angle;X_Aix_GSE_lon,X_Aix_GSE_lat,Y_Axis_GSE_lon,Y_Axis_GSE_lat,
   Z_Axis_GSE_lon,Z_Axis_GSE_lat(Degree)
   
   
   
   

